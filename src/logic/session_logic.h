#ifndef CONNECTION_LOGIC_H
#define CONNECTION_LOGIC_H
#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <kma_account.h>
#include <kma_auth.h>

#include "../utils.h"

#include "../persistence/session_persistence.h"

typedef struct keno_session
{
    kma_conn* conn;
    char* handle;
} Session;

// ---------- CREATION/DELETION ------------

// --- Memory operations ---

// Allocates duplicate of a given session
Session*
session_duplicate(const Session* session);
// Deallocates a session from memory
void
destroy_session(Session* session);
// Deallocates a session list from memory
void
destroy_session_list(Session** list);

// --- Session store operations ---
// Single
int
session_store_add(Session* session);
int
session_store_remove(char* handle);
// Whole list
Session**
session_store_load();

// --- By process ---

// 1. Create from domain
Session*
create_session(const char* domain);
// 2. Assemble URL to get code
char*
get_auth_url(Session* session);
// 3. Send code to get user token
int
get_token(Session* session, const char* code);

// -------------- OPERATIONS --------------

// Search for session in list by handle. Returns -1 on error
int
search_session(Session** list, char* handle);

#endif // CONNECTION_LOGIC_H
