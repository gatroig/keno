#include "post_logic.h"
#include "kma_account.h"
#include "kma_status.h"
#include "src/utils.h"

Post*
get_post_by_id(Session* sess, char* id)
{
    kma_status* status = kma_getStatus(sess->conn, id);
    return post_from_kma_status(status);
}

Post*
post_from_kma_status(kma_status* status)
{
    Post* post = malloc(sizeof(Post));
    if (post == NULL) {
        keno_error(KENO_ERR_MEMORY, "Couldn¥t allocate space for post.");
        return NULL;
    }

    // The ID is always that of the post, doesn't matter if the "post" is really
    // a status.
    post->id = strdup(status->id);

    // The mastodon API treats reblogs as a Status children of the main Status.
    // We don't want that.
    // If the status is a reblog, make the reblogged status the main one.
    if (status->reblog == NULL) {
        // FIXME: Don't do this. Do it properly.
        kma_statusFormat(status);

        post->status = status;
        post->reblogged_by = NULL;
    } else {
        // FIXME: Don't do this. Do it properly.
        kma_statusFormat(status->reblog);

        post->status = status->reblog;
        post->reblogged_by = status->account;

        // Assigning the child status like this would mean a leak (of the parent
        // status). To fix this, free it putting the used children as NULL, so
        // it won't be freed.
        status->reblog = NULL;
        status->account = NULL;
        kma_destroyStatus(status);
    }

    return post;
}

int
send_reply(const Session* session,
           const char* in_reply_to_id,
           const char* spoiler_text,
           const char* text,
           uint8_t sensible,
           const char* visibility)
{
    // TODO: Check for NULLs
    kma_status* status = kma_postStatus(
      session->conn, text, in_reply_to_id, 0, spoiler_text, visibility);
    if (status == NULL) {
        return KENO_ERROR;
    } else {
        return KENO_OK;
    }
}

// Send a (non reply) post
// REVIEW Should this be a public method, or not worth it?
int
send_post(const Session* session,
          const char* spoiler_text,
          const char* text,
          uint8_t sensible,
          const char* visibility)
{
    return send_reply(session, spoiler_text, NULL, text, sensible, visibility);
}

void
destroy_post(Post* post)
{
    if (post != NULL) {
        if (post->id != NULL) {
            free(post->id);
            post->id = NULL;
        }

        kma_destroyStatus(post->status);
        post->status = NULL;

        kma_destroyAccount(post->reblogged_by);
        post->reblogged_by = NULL;

        free(post);
    }
}
