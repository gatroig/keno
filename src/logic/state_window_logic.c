#include "state_window_logic.h"

struct _StateWindow
{
    GtkApplicationWindow parent;
    Session* active_session;
};

G_DEFINE_TYPE(StateWindow, state_window, GTK_TYPE_APPLICATION_WINDOW);

static void
state_window_dispose(GObject* gobject)
{
    StateWindow* statewin = KENO_STATE_WINDOW(gobject);
    if (statewin->active_session) {
        destroy_session(statewin->active_session);
        statewin->active_session = NULL;
    }

    G_OBJECT_CLASS(state_window_parent_class)->dispose(gobject);
}

static void
state_window_init(StateWindow* statewin)
{
    statewin->active_session = NULL;

    // Set default selected account
    Session** list = session_store_load();
    if (list) {
        if (list[0]) {
            state_window_set_active_session(statewin, list[0]);
        }
    } else {
        keno_error(KENO_ERR_NULL,
                   "Couldn't change session, store list returned NULL.");
    }
}

static void
state_window_class_init(StateWindowClass* class)
{
    GObjectClass* object_class = G_OBJECT_CLASS(class);

    object_class->dispose = state_window_dispose;

    g_signal_new("active_account_changed",
                 TYPE_STATE_WINDOW,
                 G_SIGNAL_RUN_FIRST,
                 0,
                 NULL,
                 NULL,
                 NULL,
                 G_TYPE_NONE,
                 0);

    g_signal_new("account_added",
                 TYPE_STATE_WINDOW,
                 G_SIGNAL_RUN_FIRST,
                 0,
                 NULL,
                 NULL,
                 NULL,
                 G_TYPE_NONE,
                 0);

    g_signal_new("account_removed",
                 TYPE_STATE_WINDOW,
                 G_SIGNAL_RUN_FIRST,
                 0,
                 NULL,
                 NULL,
                 NULL,
                 G_TYPE_NONE,
                 0);
}
GtkWidget*
state_window_new(GtkWidget* root)
{
    StateWindow* statewin = g_object_new(TYPE_STATE_WINDOW, NULL);

    return GTK_WIDGET(statewin);
}

// ---------- ACTIVE SESSION MANAGEMENT ------------

const Session*
state_window_get_active_session(const StateWindow* statewin)
{
    if (!statewin) {
        keno_error(KENO_ERR_NULL,
                   "Tried to get active session from NULL state window");
        return NULL;
    }

    return statewin->active_session;
}

int
state_window_set_active_session(StateWindow* statewin, Session* session)
{
    if (!statewin) {
        keno_error(KENO_ERR_NULL,
                   "Tried to set active session with NULL state window");
        return KENO_ERROR;
    }

    if (!session || !session->handle) {
        keno_error(
          KENO_ERR_NULL,
          "Tried to set NULL pointer as the active session. Aborting.");
        return KENO_ERROR;
    }

    // Session set is NULL or session was the one already being set
    if (statewin->active_session &&
        !strcmp(session->handle, statewin->active_session->handle)) {
        return KENO_OK;
    }

    // If there was a previous active session, destroy it. We own it, it's safe.
    if (statewin->active_session) {
        destroy_session(statewin->active_session);
        statewin->active_session = NULL;
    }

    statewin->active_session = session;

    g_signal_emit_by_name(statewin, "active_account_changed");

    return KENO_OK;
}
