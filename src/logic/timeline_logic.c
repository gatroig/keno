#include "timeline_logic.h"
#include "src/logic/post_logic.h"
#include "src/utils.h"

#include <kma_timeline.h>
#include <stdlib.h>

static Timeline*
__status_array_to_timeline(kma_status** status_array)
{
    if (status_array == NULL) {
        keno_error(KENO_ERR_MEMORY, "Failed to fetch post array.");
        return NULL;
    }

    /* Counting the number of statuses returned */

    int count = 0;
    while (status_array[count] != NULL) {
        count++;
    }

    /* Assigning status to Post structure */

    Post** post_array = calloc(count, sizeof(Post*));
    if (post_array == NULL) {
        keno_error(KENO_ERR_MEMORY, "Post Array malloc failed");
        kma_destroyTL(status_array);
        return NULL;
    }

    for (int i = 0; i < count; i++) {
        post_array[i] = post_from_kma_status(status_array[i]);
        if (post_array[i] == NULL) {
            keno_error(KENO_ERR_MEMORY, "Timeline malloc failed");
            // Normally to deallocate an array of posts we would use
            // destroy_post(), but int this case the array is not finished, so
            // we can't call that. Deallocate it separately
            kma_destroyTL(status_array);
            // Free the posts allocated until now
            for (int j = 0; j < i; j++) {
                destroy_post(post_array[i]);
            }
            free(post_array);
            return NULL;
        }
    }

    /* Assigning Post array to Timeline structure */

    Timeline* return_timeline = malloc(sizeof(Timeline));
    if (return_timeline == NULL) {
        for (int i = 0; i < count; i++) {
            destroy_post(post_array[i]);
        }
        free(post_array);
        keno_error(KENO_ERR_MEMORY, "Couldn't allocate space for timeline.");
        return NULL;
    }

    return_timeline->post_list = post_array;
    return_timeline->count = count;
    return_timeline->type = TL_HOME;

    // TODO: Implement cleanup with a "goto"

    return return_timeline;
}

static inline int
__check_timeline(const Session* sess, int limit)
{
    /* Check parameters */
    if (sess == NULL || sess->conn == NULL) {
        keno_error(KENO_ERR_NULL, "Couldn't get timeline, NULL session passed.");
        return KENO_ERROR;
    }
    if (limit < 1) {
        keno_warning(KENO_ERR_OTHER, "Tried to load timeline with a limit < 1.");
        return KENO_ERROR;
    }

    return KENO_OK;
}

Timeline*
get_federated_timeline(const Session* sess,
                       const char* max_id,
                       const char* min_id,
                       int limit)
{
    if (__check_timeline(sess, limit) != KENO_OK)
        return NULL;

    return __status_array_to_timeline(
      kma_getFederatedTL(sess->conn, max_id, NULL, min_id, limit));
}

Timeline*
get_local_timeline(const Session* sess,
                   const char* max_id,
                   const char* min_id,
                   int limit)
{
    if (__check_timeline(sess, limit) != KENO_OK)
        return NULL;

    return __status_array_to_timeline(
      kma_getLocalTL(sess->conn, max_id, NULL, min_id, limit));
}

Timeline*
get_home_timeline(const Session* sess,
                  const char* max_id,
                  const char* min_id,
                  int limit)
{
    if (__check_timeline(sess, limit) != KENO_OK)
        return NULL;

    return __status_array_to_timeline(
      kma_getHomeTL(sess->conn, max_id, NULL, min_id, limit));
}

void
destroy_timeline(Timeline* timeline)
{
    if (timeline != NULL) {
        for (int i = 0; i < timeline->count; i++) {
            destroy_post(timeline->post_list[i]);
        }
        free(timeline);
    }
}
