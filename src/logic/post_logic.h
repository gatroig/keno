#ifndef POST_LOGIC_H_
#define POST_LOGIC_H_

#include "session_logic.h"
#include <kma_status.h>

typedef struct
{
    kma_status* status;
    char* id;
    kma_account* reblogged_by;
} Post;

Post*
get_post_by_id(Session* sess, char* id);

Post*
post_from_kma_status(kma_status* status);

int
send_post(const Session* session,
          const char* spoiler_text,
          const char* text,
          uint8_t sensible,
          const char* visibility);

int
send_reply(const Session* session,
           const char* in_reply_to_id,
           const char* spoiler_text,
           const char* text,
           uint8_t sensible,
           const char* visibility);

void
destroy_post(Post* post);

#endif // POST_LOGIC_H_
