#ifndef STATE_WINDOW_LOGIC_H_
#define STATE_WINDOW_LOGIC_H_

#include <gtk/gtk.h>

#include "../utils.h"
#include "session_logic.h"

/**
 * Object to store the state of the application, as well as signal handler for
 * global events (like account switch, account logout, etc.)
 *
 * @signal active_account_changed, account_added, account_removed
 *
 */

#define TYPE_STATE_WINDOW state_window_get_type()
G_DECLARE_FINAL_TYPE(StateWindow,
                     state_window,
                     KENO,
                     STATE_WINDOW,
                     GtkApplicationWindow)

GtkWidget*
state_window_new(GtkWidget* root);

/**
 * Get the active session.
 * Data should not be freed, it is owned by the session.
 * @return Session pointer. NULL on error.
 */
const Session*
state_window_get_active_session(const StateWindow* statewin);

/**
 * Set the current selected active Keno session. The passed session memory will
 * be property of the window state. The previous session memory will be freed at
 * this point
 *
 * @signal Will emit "account_changed" signal.
 * @return Returns nonzero on error
 */
int
state_window_set_active_session(StateWindow* statewin, Session* session);

#endif // STATE_WINDOW_LOGIC_H_
