#ifndef TIMELINE_LOGIC_H_
#define TIMELINE_LOGIC_H_

#include "post_logic.h"

enum tl_type
{
    TL_OTHER,
    TL_HOME,
    TL_LOCAL,
    TL_FEDERATED,
    TL_THREAD
};

typedef struct
{
    Post** post_list;
    int count;
    enum tl_type type;
} Timeline;

// Common interface for the timeline functions
typedef Timeline* (*TimelineFunction)(const Session*,
                                      const char*,
                                      const char*,
                                      int);

Timeline*
get_federated_timeline(const Session* sess,
                       const char* max_id,
                       const char* min_id,
                       int limit);

Timeline*
get_local_timeline(const Session* sess,
                   const char* max_id,
                   const char* min_id,
                   int limit);

Timeline*
get_home_timeline(const Session* sess,
                  const char* max_id,
                  const char* min_id,
                  int limit);

void
destroy_timeline(Timeline* timeline);

#endif // TIMELINE_LOGIC_H_
