#include "session_logic.h"

static Session*
__allocate_session()
{
    Session* session = malloc(sizeof(Session*));
    if (!session) {
        keno_error(KENO_ERR_MEMORY, "Failed to allocate session.");
        return NULL;
    }
    session->conn = malloc(sizeof(kma_conn));
    if (!session) {
        keno_error(KENO_ERR_MEMORY, "Failed to allocate session's conn.");
        return NULL;
    }

    return session;
}

/**
 * Initializes the session table if it wasn't.
 * @return Returns wether it has been initialized properly or it was already
 *         initalized
 */
int
__db_session_init()
{
    sqlite3* db = get_db();
    if (!db) {
        sqlite3_close(db);
        keno_error(KENO_ERR_FILE, "Couldn't initialize Sessions table.");
        return KENO_ERROR;
    }

    int ret =
      sqlite3_exec(db,
                   "CREATE TABLE IF NOT EXISTS session ("
                   "handle TEXT UNIQUE, app_token TEXT, client_id TEXT, "
                   "client_secret TEXT, instance TEXT, user_token TEXT"
                   ")",
                   NULL,
                   NULL,
                   NULL);
    sqlite3_close(db);

    if (ret != SQLITE_OK) {
        keno_error(KENO_ERR_FILE, "Couldn't initialize Sessions table.");
        return KENO_ERROR;
    }

    return KENO_OK;
}

int
session_store_add(Session* session)
{
    if (!session || !session->conn || !session->handle) {
        keno_error(KENO_ERR_NULL, "Invalid session, couldn't save.");
        return KENO_ERROR;
    }

    // Create session schema if it doesn't exist already
    __db_session_init();

    // Open database
    sqlite3* db = get_db();
    if (!db) {
        sqlite3_close(db);
        keno_error(KENO_ERR_FILE, "Could't open database to save the session.");
        return KENO_ERROR;
    }

    sqlite3_stmt* stmt;
    int ret = sqlite3_prepare_v2(
      db,
      "INSERT INTO session (handle, app_token, client_id, client_secret, "
      "instance, user_token) VALUES (?, ?, ?, ?, ?, ?);",
      -1,
      &stmt,
      NULL);
    if (ret != SQLITE_OK) {
        sqlite3_finalize(stmt);
        sqlite3_close(db);
        keno_error(KENO_ERR_OTHER,
                   "Couldn't insert user session into database.");
        return KENO_ERROR;
    }

    sqlite3_bind_text(stmt, 1, session->handle, -1, SQLITE_STATIC);
    sqlite3_bind_text(stmt, 2, session->conn->app_token, -1, SQLITE_STATIC);
    sqlite3_bind_text(stmt, 3, session->conn->client_id, -1, SQLITE_STATIC);
    sqlite3_bind_text(stmt, 4, session->conn->client_secret, -1, SQLITE_STATIC);
    sqlite3_bind_text(stmt, 5, session->conn->instance, -1, SQLITE_STATIC);
    sqlite3_bind_text(stmt, 6, session->conn->user_token, -1, SQLITE_STATIC);

    // Execute query
    sqlite3_step(stmt);

    // Get status
    ret = sqlite3_extended_errcode(db);

    sqlite3_finalize(stmt);
    sqlite3_close(db);

    // Check for errors with above returned status
    if (ret != SQLITE_DONE) {
        if (ret == SQLITE_CONSTRAINT_UNIQUE) {
            keno_error(KENO_ERR_CONSTRAINT, "User already exists.");
        } else {
            keno_error(KENO_ERR_OTHER, "Couldn't add session to the database");
        }
        return KENO_ERROR;
    }

    return KENO_OK;
}

int
session_store_remove(char* handle)
{
    if (!handle) {
        keno_error(KENO_ERR_NULL, "Couldn't remove session, handle is NULL");
        return KENO_ERROR;
    }

    sqlite3* db = get_db();
    sqlite3_stmt* stmt = NULL;

    int ret = sqlite3_prepare_v2(
      db, "DELETE FROM session WHERE handle = ?;", -1, &stmt, NULL);

    if (ret != SQLITE_OK) {
        sqlite3_close(db);
        sqlite3_finalize(stmt);
        keno_error(KENO_ERR_OTHER, "Couldn't remove session, SQLITE error.");
        return KENO_ERROR;
    }

    sqlite3_bind_text(stmt, 1, handle, -1, NULL);
    ret = sqlite3_step(stmt);

    sqlite3_close(db);
    sqlite3_finalize(stmt);

    if (ret != SQLITE_DONE) {
        keno_error(KENO_ERR_OTHER,
                   "Could not remove session, error in SQL transaction");
        return KENO_ERROR;
    }

    if (sqlite3_changes(db) < 1) {
        keno_warning(
          KENO_ERR_OTHER,
          "Attempted to delete session not in store. No rows affected.");
    }

    return KENO_OK;
}

static int
__get_session_store_count()
{
    sqlite3* db = get_db();
    sqlite3_stmt* stmt = NULL;

    // Prepare statement
    int ret =
      sqlite3_prepare_v2(db, "SELECT COUNT(*) FROM session;", -1, &stmt, NULL);
    if (ret != SQLITE_OK) {
        sqlite3_finalize(stmt);
        sqlite3_close(db);
        keno_error(
          KENO_ERR_OTHER,
          "Couldn't prepare SQL statement to retrieve session store count.");
        return -1;
    }

    // Check we got something the value
    if (sqlite3_step(stmt) != SQLITE_ROW) {
        sqlite3_finalize(stmt);
        sqlite3_close(db);
        keno_error(KENO_ERR_OTHER,
                   "Couldn't get row containing amount of sessions in store");
        return -1;
    }

    int count = sqlite3_column_int(stmt, 0);
    sqlite3_finalize(stmt);
    sqlite3_close(db);
    return count;
}

Session**
session_store_load()
{
    __db_session_init();

    int count = __get_session_store_count();
    if (count < 0) {
        keno_error(KENO_ERR_OTHER,
                   "Couldn't get store count. Session store list load failed.");
        return NULL;
    }

    sqlite3* db = get_db();
    sqlite3_stmt* stmt = NULL;

    int ret =
      sqlite3_prepare_v2(db,
                         "SELECT handle, app_token, client_id, client_secret, "
                         "instance, user_token FROM session;",
                         -1,
                         &stmt,
                         NULL);
    if (ret != SQLITE_OK) {
        sqlite3_finalize(stmt);
        sqlite3_close(db);
        keno_error(KENO_ERR_OTHER,
                   "Couldn't prepare SQL statement to retrieve session list.");
        return NULL;
    }

    // +1 for delimiting NULL position
    Session** list = malloc((count + 1) * sizeof(Session*));

    if (!list) {
        sqlite3_finalize(stmt);
        sqlite3_close(db);
        keno_error(KENO_ERR_MEMORY, "Couldn't allocate space for session list");
        return NULL;
    }

    // Fill the Session
    for (int i = 0; i < count; i++) {
        // Check we got info
        if (sqlite3_step(stmt) != SQLITE_ROW) {
            keno_error(KENO_ERR_OTHER,
                       "Session list retrieval returned less columns than the "
                       "ones counted beforehand. Breaking early.");
            break;
        }

        // Make space for the conn
        list[i] = __allocate_session();
        if (!list[i]->conn) {
            sqlite3_finalize(stmt);
            sqlite3_close(db);
            for (int j = 0; list[j] != NULL; j++) {
                destroy_session(list[j]);
            }
            free(list);
            keno_error(KENO_ERR_MEMORY, "Failed to allocate session's conn.");
            return NULL;
        }

        // Retrieve the data
        list[i]->handle = strdup((const char*)sqlite3_column_text(stmt, 0));
        list[i]->conn->app_token =
          strdup((const char*)sqlite3_column_text(stmt, 1));
        list[i]->conn->client_id =
          strdup((const char*)sqlite3_column_text(stmt, 2));
        list[i]->conn->client_secret =
          strdup((const char*)sqlite3_column_text(stmt, 3));
        list[i]->conn->instance =
          strdup((const char*)sqlite3_column_text(stmt, 4));
        list[i]->conn->user_token =
          strdup((const char*)sqlite3_column_text(stmt, 5));
    }

    list[count] = NULL;
    sqlite3_finalize(stmt);
    sqlite3_close(db);

    return list;
}

/** Safe wrapper around strdup so it doesn't segfault on NULL */
static char*
__strdup_s(const char* string)
{
    if (string)
        return strdup(string);
    return NULL;
}

Session*
session_duplicate(const Session* session)
{
    if (!session || !session->conn) {
        keno_error(KENO_ERR_NULL,
                   "Couldn't duplicate session. NULL session/session "
                   "connection passed.");
        return NULL;
    }

    Session* new_session = __allocate_session();
    if (!new_session) {
        keno_error(KENO_ERR_MEMORY,
                   "Couldn't allocate space for duplicate of Session.");
        return NULL;
    }

    new_session->handle = __strdup_s(session->handle);
    new_session->conn->instance = __strdup_s(session->conn->instance);
    new_session->conn->app_token = __strdup_s(session->conn->app_token);
    new_session->conn->client_id = __strdup_s(session->conn->client_id);
    new_session->conn->client_secret = __strdup_s(session->conn->client_secret);
    new_session->conn->user_token = __strdup_s(session->conn->user_token);

    return new_session;
}

void
destroy_session(Session* session)
{
    if (session) {
        if (session->conn) {
            kma_destroyConn(session->conn);
            session->conn = NULL;
        }
        if (session->handle) {
            free(session->handle);
            session->handle = NULL;
        }
        free(session);
    }
    session = NULL;
}

int
search_session(Session** list, char* handle)
{
    if (!list)
        return -1;

    for (int i = 0; list[i] != NULL; i++) {
        if (strcmp(list[i]->handle, handle) == 0) {
            return i;
        }
    }

    return -1;
}

void
destroy_session_list(Session** list)
{
    if (!list)
        return;

    for (int i = 0; list[i] != NULL; i++) {
        destroy_session(list[i]);
        list[i] = NULL;
    }
    free(list);
}

Session*
create_session(const char* domain)
{
    if (!domain) {
        keno_error(KENO_ERR_NULL,
                   "Couldn't create sesion, NULL instance string passed.");
        return NULL;
    }
    Session* session = malloc(sizeof(Session));
    if (!session) {
        keno_error(KENO_ERR_MEMORY, "Couldn't allocate space for session.");
        return NULL;
    }
    // First put everything as NULL, prevents segfault at freeing
    session->handle = NULL;

    // Populate libkenoma's connection
    char* url = NULL;
    asprintf(&url, "https://%s", domain);
    session->conn = kma_createConn(url,
                                   "read write",
                                   KMA_DEFAULT_REDIRECT,
                                   APPLICATION_NAME,
                                   APPLICATION_WEBSITE);

    free(url);
    if (!session->conn) {
        // Error already thrown by libkenoma
        destroy_session(session);
        return NULL;
    }

    // The rest of the Session part cannot be loaded just yet because the
    // created connection doesn't have a user token

    return session;
}

char*
get_auth_url(Session* session)
{
    if (!session || !session->conn) {
        keno_error(KENO_ERR_NULL,
                   "Attempted to get authorization URL from NULL.");
        return NULL;
    }

    return kma_getAuthURL(
      session->conn, "read write", KMA_DEFAULT_REDIRECT, KMA_NOT_FORCE_LOGIN);
}

// This assumes you've check validities before.
static char*
__get_handle(Session* session, kma_account* account)
{
    char* domain;
    char* needle = "://"; // Save it to get strlen afterwards
    char* find = strstr(session->conn->instance, needle);
    if (find != NULL)
        // Remove the "://" part
        domain = &find[strlen(needle)];
    else
        return NULL;

    // No need to free handle, it will be handled by destroy_session()
    // Haha, handled, get it?
    char* handle = NULL;
    asprintf(&handle, "%s@%s", account->username, domain);

    // If asprintf failed handle will be NULL, so no need to check.
    return handle;
}

int
get_token(Session* session, const char* code)
{
    if (!session || !session->conn) {
        keno_error(KENO_ERR_NULL, "Attempted to get user token from NULL.");
        return 0;
    }

    kma_getUserToken(
      session->conn, "read write", KMA_DEFAULT_REDIRECT, (char*)code);

    if (session->conn->user_token) {
        // Now that we have the token we can populate the rest of the session
        kma_account* account = kma_getLoggedAccount(session->conn);
        if (account == NULL) {
            keno_error(KENO_ERR_OTHER,
                       "Couldn't retrieve user information from token");
            return 0;
        }

        // Get user handle
        char* handle = __get_handle(session, account);
        kma_destroyAccount(account);

        if (handle != NULL)
            session->handle = handle;
        else {
            keno_error(KENO_ERR_OTHER,
                       "Couldn't assemble user handle form token.");
            return 0;
        }

        return 1;
    }

    return 0;
}
