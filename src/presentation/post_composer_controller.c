#include "post_composer_controller.h"
#include "kma_status.h"
#include "src/logic/post_logic.h"
#include "src/logic/state_window_logic.h"
#include "src/presentation/session_manager.h"
#include "src/utils.h"

static GtkWidget* window = NULL;
static GtkWidget* entry_compose_spoiler_text = NULL;
static GtkWidget* textview_compose_text = NULL;
static GtkWidget* button_submit_post = NULL;
static GtkWidget* button_exit = NULL;
static GtkWidget* combobox_visibility = NULL;
/* static GtkWidget* button_add_image = NULL; */
/* static GtkWidget* button_add_poll = NULL; */

static const char* reply_to = NULL;
static const char* spoiler_text = NULL;
static const char* body_text = NULL;
static char* visibility =
  KMA_STATUS_VISIBILITY_PUBLIC; // TODO: Implement API user preference
static const Session* session = NULL;

void
__on_close_request(gpointer data)
{
    gtk_window_close(GTK_WINDOW(window));
}

void
__on_submit_button_clicked(gpointer data)
{

    spoiler_text =
      gtk_editable_get_text(GTK_EDITABLE(entry_compose_spoiler_text));

    GtkTextBuffer* buffer =
      gtk_text_view_get_buffer(GTK_TEXT_VIEW(textview_compose_text));
    GtkTextIter start, end;
    gtk_text_buffer_get_bounds(buffer, &start, &end);
    body_text = gtk_text_buffer_get_text(buffer, &start, &end, FALSE);

    int combobox_index =
      gtk_combo_box_get_active(GTK_COMBO_BOX(combobox_visibility));
    switch (combobox_index) {
        case 0:
            visibility = KMA_STATUS_VISIBILITY_PUBLIC;
            break;
        case 1:
            visibility = KMA_STATUS_VISIBILITY_UNLISTED;
            break;
        case 2:
            visibility = KMA_STATUS_VISIBILITY_PRIVATE;
            break;
        case 3:
            visibility = KMA_STATUS_VISIBILITY_DIRECT;
            break;
    }

    if (send_reply(session, reply_to, spoiler_text, body_text, 0, visibility) ==
        KENO_OK) {
        gtk_window_close(GTK_WINDOW(window));
    } else {
        return;
        puts("penis");
        // TODO inform user of error
    }
}
void
post_composer_init(GtkWidget* parent, Post* reply_post)
{
    if (parent == NULL) {
        keno_error(KENO_ERR_NULL, "Post composer window could not get parent");
        return;
    }
    if (reply_post != NULL) {
        reply_to = reply_post->id;
    }

    session = state_window_get_active_session(KENO_STATE_WINDOW(parent));

    if (session == NULL) {
        keno_error(KENO_ERR_NULL, "Post composer window could not get session");
        return;
    }

    GtkBuilder* builder =
      gtk_builder_new_from_resource("/st/fai/keno/ui/compose/compose.ui");

    window = GTK_WIDGET(gtk_builder_get_object(builder, "window"));
    entry_compose_spoiler_text =
      GTK_WIDGET(gtk_builder_get_object(builder, "entry_compose_spoiler_text"));
    textview_compose_text =
      GTK_WIDGET(gtk_builder_get_object(builder, "textview_compose_text"));
    button_submit_post =
      GTK_WIDGET(gtk_builder_get_object(builder, "button_submit_post"));
    button_exit = GTK_WIDGET(gtk_builder_get_object(builder, "button_exit"));
    combobox_visibility =
      GTK_WIDGET(gtk_builder_get_object(builder, "combobox_visibility"));
    // TODO: SET VISIBILITY according to user setting
    gtk_combo_box_set_active(GTK_COMBO_BOX(combobox_visibility), 0);

    g_object_unref(builder);

    g_signal_connect(button_submit_post,
                     "clicked",
                     G_CALLBACK(__on_submit_button_clicked),
                     NULL);

    g_signal_connect(
      button_exit, "clicked", G_CALLBACK(__on_close_request), NULL);

    gtk_window_set_transient_for(GTK_WINDOW(window), GTK_WINDOW(parent));
    gtk_window_set_modal(GTK_WINDOW(window), TRUE);
    gtk_widget_show(window);

    g_signal_connect(
      window, "close-request", G_CALLBACK(__on_close_request), NULL);

    if (reply_post != NULL) {

        char* reply_string =
          g_strconcat("@", reply_post->status->account->acct, NULL);
        gtk_text_buffer_set_text(
          gtk_text_view_get_buffer(GTK_TEXT_VIEW(textview_compose_text)),
          reply_string,
          -1);
        g_free(reply_string);
    }
}
