#ifndef TIMELINE_CONTAINER_H_
#define TIMELINE_CONTAINER_H_

#include <gtk/gtk.h>

#include "../logic/state_window_logic.h"
#include "../logic/timeline_logic.h"

#define TYPE_TIMELINE_CONTAINER timeline_container_get_type()

G_DECLARE_FINAL_TYPE(TimelineContainer,
                     timeline_container,
                     KENO,
                     TIMELINE_CONTAINER,
                     GtkBox)

GtkWidget*
timeline_container_new(GtkWidget* root, TimelineFunction timeline);

void
timeline_container_reload(TimelineContainer* tlcont);
void
timeline_container_clear(TimelineContainer* tlcont);

#endif // TIMELINE_CONTAINER_H_
