#include "session_manager.h"
#include "session_manager_row.h"

struct _SessionManager
{
    GtkBox parent;
    GtkWidget* row_container;
    GtkWidget* button_add;

    Session** session_list;

    // TEMPORAL
    GtkWidget* button_reload;
};

G_DEFINE_TYPE(SessionManager, session_manager, GTK_TYPE_BOX);

static void
session_manager_dispose(GObject* gobject)
{
    // SessionManager* sessman = KENO_SESSION_MANAGER(gobject);
    G_OBJECT_CLASS(session_manager_parent_class)->dispose(gobject);
}

static void
session_manager_init(SessionManager* sessman)
{
    sessman->row_container = NULL;
    sessman->button_add = NULL;
    sessman->button_reload = NULL;
    sessman->session_list = NULL;

    gtk_widget_init_template(GTK_WIDGET(sessman));
}

static void
session_manager_class_init(SessionManagerClass* class)
{
    GObjectClass* object_class = G_OBJECT_CLASS(class);

    object_class->dispose = session_manager_dispose;

    // Get template
    gtk_widget_class_set_template_from_resource(
      GTK_WIDGET_CLASS(class), "/st/fai/keno/ui/session/session_manager.ui");
    // Bind template objects
    gtk_widget_class_bind_template_child(
      GTK_WIDGET_CLASS(class), SessionManager, row_container);
    gtk_widget_class_bind_template_child(
      GTK_WIDGET_CLASS(class), SessionManager, button_add);
    gtk_widget_class_bind_template_child(
      GTK_WIDGET_CLASS(class), SessionManager, button_reload);
}

void
session_manager_clear(SessionManager* sessman)
{
    if (!sessman) {
        keno_error(KENO_ERR_NULL, "Tried to clear NULL session manager.");
        return;
    }

    GtkListBox* list = GTK_LIST_BOX(sessman->row_container);

    // Remove each SessionManagerRow
    for (int i = 0; gtk_list_box_get_row_at_index(list, 0) != NULL; i++) {
        GtkListBoxRow* child = gtk_list_box_get_row_at_index(list, 0);
        gtk_list_box_remove(list, GTK_WIDGET(child));
    }

    // Free the SessionList
    if (sessman->session_list) {
        destroy_session_list(sessman->session_list);
        sessman->session_list = NULL;
    }
}

void
session_manager_reload(SessionManager* sessman)
{
    if (!sessman) {
        keno_error(KENO_ERR_NULL, "Tried to reload NULL session manager.");
        return;
    }

    // Clear session list. Make sure there's nothing there.
    session_manager_clear(sessman);
    if (sessman->session_list != NULL) {
        keno_warning(KENO_ERR_OTHER,
                     "Reloaded session manager with previous data. Something "
                     "went wront clearing");
    }

    // Get array of Sessions and populate GtkListBox
    sessman->session_list = session_store_load();

    // Deactivate the add accout button on error
    // (there isn't access to the database for some reason)
    if (sessman->session_list == NULL) {
        gtk_widget_set_sensitive(sessman->button_add, 0);
        return;
    }

    // Initialize each row based on the retrieved sessions
    for (int i = 0; sessman->session_list[i] != NULL; i++) {
        GtkWidget* row = session_manager_row_new(sessman);
        gtk_list_box_append(GTK_LIST_BOX(sessman->row_container), row);
        session_manager_row_reload(KENO_SESSION_MANAGER_ROW(row));
    }
}

static void
__reload_view(GtkWidget* caller, SessionManager* sessman)
{
    session_manager_reload(sessman);
}

static void
__on_select(GtkWidget* widget, GtkListBoxRow* row, gpointer sessman_gp)
{
    if (row) {
        // To retrieve the Session, we need the row index
        int index = gtk_list_box_row_get_index(row);
        if (index < 0) {
            keno_error(KENO_ERR_NULL,
                       "Couldn't select account, row selected is unparented.");
            return;
        }

        // Use that index to get the Session at offset in session_list
        SessionManager* sessman = KENO_SESSION_MANAGER(sessman_gp);
        Session* session = session_manager_get_session_idx(sessman, index);
        if (!session) {
            keno_error(KENO_ERR_NULL,
                       "Session selected is NULL, aborting session change.");
            return;
        }

        // Get a copy of that so we can take ownership and asign it
        Session* session_dup = session_duplicate(session);
        if (!session_dup) {
            keno_error(
              KENO_ERR_MEMORY,
              "Couldn't allocate space for session changing. Aborting.");
            return;
        }
        state_window_set_active_session(
          KENO_STATE_WINDOW(gtk_widget_get_root(widget)), session_dup);
        session_dup = NULL;
    }
}

GtkWidget*
session_manager_new(GtkWidget* root)
{
    SessionManager* sessman = g_object_new(TYPE_SESSION_MANAGER, NULL);

    session_manager_reload(sessman);

    g_signal_connect(
      sessman->button_add, "clicked", G_CALLBACK(add_session_init), root);

    // TEMPORARY
    g_signal_connect(
      sessman->button_reload, "clicked", G_CALLBACK(__reload_view), sessman);

    g_signal_connect(
      sessman->row_container, "row-selected", G_CALLBACK(__on_select), sessman);

    // When adding or removing accounts
    g_signal_connect(root, "account_added", G_CALLBACK(__reload_view), sessman);
    g_signal_connect(
      root, "account_removed", G_CALLBACK(__reload_view), sessman);

    return GTK_WIDGET(sessman);
}

Session*
session_manager_get_session_idx(const SessionManager* sessman, int index)
{
    if (!sessman) {
        keno_error(KENO_ERR_NULL,
                   "Tried to get session by index from NULL SessionManager.");
        return NULL;
    }

    if (!sessman->session_list) {
        keno_error(KENO_ERR_NULL,
                   "Tried to get session by index from SessionManager with "
                   "uninitializd session_list.");
        return NULL;
    }

    // Find out if the index is out of bounds
    for (int i = 0; i <= index; i++) {
        if (!sessman->session_list[i]) {
            keno_error(KENO_ERR_NULL,
                       "Tried to get an out of bound value from session list");
            return NULL;
        }
    }

    return sessman->session_list[index];
}
