#include "timeline_container.h"
#include "src/utils.h"
#include "timeline_post.h"

// TODO: This will be chosen in the user prefrences
#define KENO_TIMELINE_POSTS_PER_PAGE 10

struct _TimelineContainer
{
    GtkBox parent;
    GtkWidget* post_container;
    GtkWidget* spinner_timeline;
    GtkWidget* scrolled_timeline;
    GCancellable* cancellable;

    TimelineFunction timeline_function;

    // We don't want to be cancelling the load-on-scroll because the user has
    // scrolled again. It is async, but we should wait until it's finished.
    int is_loading;

    // We store the oldest and newest IDs to be able to load more posts in
    // either direction because there's no easy way of accesing the IDs from the
    // GtkListBox
    char* newest_id;
    char* oldest_id;
};

G_DEFINE_TYPE(TimelineContainer, timeline_container, GTK_TYPE_BOX);

static void
timeline_container_dispose(GObject* gobject)
{
    // TimelineContainer* tlcont = KENO_TIMELINE_CONTAINER(gobject);
    G_OBJECT_CLASS(timeline_container_parent_class)->dispose(gobject);
}

static int
__set_info_done(gpointer data)
{
    TimelineContainer* tlcont = KENO_TIMELINE_CONTAINER(data);

    // REVIEW
    tlcont->cancellable = NULL;
    //

    gtk_widget_set_visible(tlcont->spinner_timeline, 0);
    gtk_widget_set_visible(tlcont->scrolled_timeline, 1);
    tlcont->is_loading = 0;

    return 0;
}

inline static void
__append_timeline(TimelineContainer* tlcont, Timeline* timeline)
{
    if (timeline->count <= 0) {
        keno_warning(KENO_ERR_OTHER, "No more posts to load.");
        tlcont->is_loading = 0;
        return;
    }

    // Update highest and lowest points in the loaded posts
    if (tlcont->newest_id == NULL ||
        strcmp(tlcont->newest_id, timeline->post_list[0]->id) < 0) {
        // New upper limit found
        free(tlcont->newest_id);
        tlcont->newest_id = strdup(timeline->post_list[0]->id);
    }
    if (tlcont->oldest_id == NULL ||
        strcmp(tlcont->oldest_id,
               timeline->post_list[timeline->count - 1]->id) > 0) {
        // New lower limit found
        free(tlcont->oldest_id);
        tlcont->oldest_id =
          strdup(timeline->post_list[timeline->count - 1]->id);
    }

    // Actually append the posts (and set them to NULL)
    for (int i = 0; i < timeline->count; i++) {
        GtkWidget* tlpost = timeline_post_new();
        gtk_list_box_append(GTK_LIST_BOX(tlcont->post_container), tlpost);
        timeline_post_make_post(KENO_TIMELINE_POST(tlpost),
                                timeline->post_list[i]);
        timeline->post_list[i] = NULL;
    }

    // The individual posts haven been set to NULL, they're not beeing free'd.
    // Owership has been transfered to the individual lines
    destroy_timeline(timeline);
}

static void
__task_set_info(GTask* task,
                gpointer source_object,
                gpointer data,
                GCancellable* cancellable)
{
    if (g_task_return_error_if_cancelled(task))
        return;
    TimelineContainer* tlcont = KENO_TIMELINE_CONTAINER(data);
    tlcont->cancellable = cancellable;

    // Get timeline from global state
    //////////

    // Any widget -> GtkRoot -> active_session -> timeline

    GtkWidget* statewin = GTK_WIDGET(gtk_widget_get_root(GTK_WIDGET(tlcont)));
    if (statewin == NULL) {
        keno_error(KENO_ERR_NULL,
                   "Can't reload timeline, failed to get app state.");
        tlcont->is_loading = 0;
        return;
    }
    const Session* session =
      state_window_get_active_session(KENO_STATE_WINDOW(statewin));
    if (session == NULL) {
        // REVIEW: There's no session active or there's no session at all.
        // This isn't an error (I think).
        tlcont->is_loading = 0;
        tlcont->cancellable = NULL;
        return;
    }
    // REVIEW TODO: Put the actual limit, not hardcoded
    Timeline* timeline = (*tlcont->timeline_function)(
      session, NULL, NULL, KENO_TIMELINE_POSTS_PER_PAGE);
    if (timeline == NULL) {
        keno_error(KENO_ERR_NULL, "Failed to fetch timeline.");
        tlcont->is_loading = 0;
        return;
    }

    // Update the list
    //////////

    timeline_container_clear(tlcont);

    // NOTE: Inline function mutates the timeline to set all its posts to NULL.
    __append_timeline(tlcont, timeline);
    timeline = NULL;

    // Free operations should be done in __set_info_done()
    g_idle_add(__set_info_done, tlcont);
}

static void
__task_load_older(GTask* task,
                  gpointer source_object,
                  gpointer data,
                  GCancellable* cancellable)
{
    if (g_task_return_error_if_cancelled(task)) {
        return;
    }
    TimelineContainer* tlcont = KENO_TIMELINE_CONTAINER(data);

    // Get timeline from global state
    //////////

    // Any widget -> GtkRoot -> active_session -> timeline

    GtkWidget* statewin = GTK_WIDGET(gtk_widget_get_root(GTK_WIDGET(tlcont)));
    if (statewin == NULL) {
        keno_error(KENO_ERR_NULL,
                   "Can't reload timeline, failed to get app state.");
        tlcont->is_loading = 0;
        return;
    }
    const Session* session =
      state_window_get_active_session(KENO_STATE_WINDOW(statewin));
    if (session == NULL) {
        // REVIEW: There's no session active or there's no session at all.
        // This isn't an error (I think).
        tlcont->is_loading = 0;
        return;
    }
    // TODO: Put the actual limit, not hardcoded
    Timeline* timeline = (*tlcont->timeline_function)(
      session, tlcont->oldest_id, NULL, KENO_TIMELINE_POSTS_PER_PAGE);
    // get_home_timeline(session, "108147964411450128", NULL, 5);
    if (timeline == NULL) {
        keno_error(KENO_ERR_NULL, "Failed to fetch timeline.");
        tlcont->is_loading = 0;
        return;
    }

    // Update the list (without a clear)
    //////////

    // NOTE: Mutates all the statuses to be set to NULL
    __append_timeline(tlcont, timeline);
    timeline = NULL;

    // Free operations should be done in __set_info_done()
    g_idle_add(__set_info_done, tlcont);
}

/**
 * REVIEW: The owner of the posts are the actual rows of posts, because of the
 *         dynamic nature of the loading of the timeline. We don't really handle
 *         that logic manually as the GtkBoxList does it ootb
 */
void
timeline_container_reload(TimelineContainer* tlcont)
{
    if (tlcont == NULL) {
        keno_error(KENO_ERR_NULL, "Tried to reload NULL container.");
        return;
    }
    tlcont->is_loading = 1;

    // Cancel previous reloading if it hasn finished

    if (tlcont->cancellable != NULL) {
        g_cancellable_cancel(tlcont->cancellable);
        return;
    }

    // Display and start loader
    gtk_widget_set_visible(tlcont->spinner_timeline, 1);
    gtk_widget_set_visible(tlcont->scrolled_timeline, 0);
    gtk_spinner_start(GTK_SPINNER(tlcont->spinner_timeline));

    // Initialize a new thread to do the actual loading
    GTask* task = g_task_new(NULL, g_cancellable_new(), NULL, tlcont);
    g_task_set_task_data(task, tlcont, NULL);
    g_task_run_in_thread(task, __task_set_info);
    g_object_unref(task);

    // Set scrollbar to top
    GtkWidget* scrollbar_aux = gtk_scrolled_window_get_vscrollbar(GTK_SCROLLED_WINDOW(tlcont->scrolled_timeline));
    GtkAdjustment* adjustment_aux = gtk_scrollbar_get_adjustment(GTK_SCROLLBAR(scrollbar_aux));
    gtk_adjustment_set_value(adjustment_aux, 0);
}

static void
timeline_container_init(TimelineContainer* tlcont)
{
    tlcont->cancellable = NULL;

    tlcont->is_loading = 0;
    tlcont->newest_id = NULL;
    tlcont->oldest_id = NULL;

    gtk_widget_init_template(GTK_WIDGET(tlcont));
}

static void
timeline_container_class_init(TimelineContainerClass* class)
{
    GObjectClass* object_class = G_OBJECT_CLASS(class);

    object_class->dispose = timeline_container_dispose;

    // Get template
    gtk_widget_class_set_template_from_resource(
      GTK_WIDGET_CLASS(class),
      "/st/fai/keno/ui/timeline/timeline_container.ui");
    // Bind template objects
    gtk_widget_class_bind_template_child(
      GTK_WIDGET_CLASS(class), TimelineContainer, post_container);
    gtk_widget_class_bind_template_child(
      GTK_WIDGET_CLASS(class), TimelineContainer, spinner_timeline);
    gtk_widget_class_bind_template_child(
      GTK_WIDGET_CLASS(class), TimelineContainer, scrolled_timeline);
}

void
timeline_container_clear(TimelineContainer* tlcont)
{
    if (tlcont == NULL)
        return;

    GtkListBox* list = GTK_LIST_BOX(tlcont->post_container);
    for (int i = 0; gtk_list_box_get_row_at_index(list, 0) != NULL; i++) {
        GtkListBoxRow* child = gtk_list_box_get_row_at_index(list, 0);
        gtk_list_box_remove(list, GTK_WIDGET(child));
    }
}

static void
__cb_account_change(GtkWidget* caller, TimelineContainer* tlcont)
{
    timeline_container_reload(tlcont);
}

static void
__cb_lower_edge(GtkScrolledWindow* self,
                GtkPositionType pos,
                TimelineContainer* tlcont)
{
    // If the user goes to the bottom two times, we don't want to load the posts
    // until we've loaded the previous ones
    if (pos == GTK_POS_BOTTOM && !tlcont->is_loading) {
        tlcont->is_loading = 1;

        // Initialize a new thread to do the actual loading
        GTask* task = g_task_new(NULL, g_cancellable_new(), NULL, tlcont);
        g_task_set_task_data(task, tlcont, NULL);
        g_task_run_in_thread(task, __task_load_older);
        g_object_unref(task);
    }
}

GtkWidget*
timeline_container_new(GtkWidget* root, TimelineFunction timeline_function)
{
    TimelineContainer* tlcont = g_object_new(TYPE_TIMELINE_CONTAINER, NULL);

    g_signal_connect(
      root, "active_account_changed", G_CALLBACK(__cb_account_change), tlcont);

    g_signal_connect(tlcont->scrolled_timeline,
                     "edge-reached",
                     G_CALLBACK(__cb_lower_edge),
                     tlcont);

    tlcont->timeline_function = timeline_function;

    return GTK_WIDGET(tlcont);
}
