#include "add_session_controller.h"
#include "src/logic/state_window_logic.h"

static GtkWidget* stack = NULL;
static GtkWidget* entry_instance = NULL;
static GtkWidget* linkbutton = NULL;
static GtkWidget* entry_code = NULL;
static GtkWidget* root_window = NULL;

static Session* session = NULL;

static void
__on_instance_submit(GtkWidget* caller, gpointer data)
{
    if (entry_instance && stack && linkbutton) {
        const char* instance_domain =
          gtk_editable_get_text(GTK_EDITABLE(entry_instance));
        session = create_session(instance_domain);
        if (session) {
            gtk_link_button_set_uri(GTK_LINK_BUTTON(linkbutton),
                                    get_auth_url(session));
            gtk_stack_set_visible_child_name(GTK_STACK(stack), "page2_web");
        } else {
            gtk_widget_add_css_class(entry_instance, "error");
            // gtk_entry_set_icon_from_icon_name(GTK_ENTRY (entry_instance),
            // GTK_ENTRY_ICON_PRIMARY, "dialog-error");
        }
    }
}

static void
__on_code_submit(GtkWidget* caller, gpointer data)
{
    const char* code = gtk_editable_get_text(GTK_EDITABLE(entry_code));

    // Try to get the token and store it. If either fails, show error.
    // TODO: Show a text with the error.
    if (get_token(session, code) && session_store_add(session)) {
        gtk_stack_set_visible_child_name(GTK_STACK(stack), "page3_finish");
        // TODO: Check if root_window is an instance of StateWindow
        // (If it isn't it doesn't explode, it just issues a warning)
        if (root_window) {
            g_signal_emit_by_name(KENO_STATE_WINDOW(root_window),
                                  "account_added");

            // Session is owned now by the state window
            state_window_set_active_session(KENO_STATE_WINDOW(root_window),
                                            session);
            session = NULL;
        } else {
            keno_error(KENO_ERR_NULL,
                       "Couldn't emit account add signal, StateWindow is null");
        }
    } else {
        gtk_widget_add_css_class(entry_code, "error");
    }
}

static void
__on_entry_type(GtkWidget* entry, gpointer data)
{
    gtk_widget_remove_css_class(entry, "error");
}

void
add_session_init(GtkWidget* caller, GtkWidget* parent)
{

    GtkBuilder* builder =
      gtk_builder_new_from_resource("/st/fai/keno/ui/session/add_session.ui");
    GtkWidget* window = GTK_WIDGET(gtk_builder_get_object(builder, "window"));
    stack = GTK_WIDGET(gtk_builder_get_object(builder, "stack"));
    // Page 1: instance
    entry_instance =
      GTK_WIDGET(gtk_builder_get_object(builder, "entry_instance"));
    GtkWidget* button_continue_instance =
      GTK_WIDGET(gtk_builder_get_object(builder, "button_continue_instance"));
    // Page 2: web
    linkbutton = GTK_WIDGET(gtk_builder_get_object(builder, "link_instance"));
    entry_code = GTK_WIDGET(gtk_builder_get_object(builder, "entry_code"));
    GtkWidget* button_continue_code =
      GTK_WIDGET(gtk_builder_get_object(builder, "button_continue_code"));
    g_object_unref(builder);

    if (parent) {
        gtk_window_set_transient_for(GTK_WINDOW(window), GTK_WINDOW(parent));
        gtk_window_set_modal(GTK_WINDOW(window), TRUE);
        //
        root_window = parent;
    } else {
        keno_error(KENO_ERR_NULL,
                   "Couldn't set transient property, NULL parent");
    }

    // Page 1
    g_signal_connect(button_continue_instance,
                     "clicked",
                     G_CALLBACK(__on_instance_submit),
                     NULL);
    g_signal_connect(
      entry_instance, "activate", G_CALLBACK(__on_instance_submit), NULL);
    // Remove error class when entry typed
    g_signal_connect(
      entry_instance, "changed", G_CALLBACK(__on_entry_type), NULL);

    // Page 2
    g_signal_connect(
      button_continue_code, "clicked", G_CALLBACK(__on_code_submit), NULL);
    g_signal_connect(
      entry_code, "activate", G_CALLBACK(__on_code_submit), NULL);
    // Remove error class when entry typed
    g_signal_connect(entry_code, "changed", G_CALLBACK(__on_entry_type), NULL);

    gtk_widget_show(window);
}
