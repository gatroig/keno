#ifndef TIMELINE_POST_H_
#define TIMELINE_POST_H_

#include "../logic/post_logic.h"
#include "post_thread.h"
#include <gtk/gtk.h>

#define TYPE_TIMELINE_POST timeline_post_get_type()
G_DECLARE_FINAL_TYPE(TimelinePost,
                     timeline_post,
                     KENO,
                     TIMELINE_POST,
                     GtkListBoxRow)

GtkWidget*
timeline_post_new();

void
timeline_post_make_post(TimelinePost* tlpost, Post* post);

#endif // TIMELINE_POST_H_
