#include "main_window_controller.h"
#include "gtk/gtkcssprovider.h"
#include "src/logic/session_logic.h"
#include "src/logic/timeline_logic.h"
#include "src/presentation/post_composer_controller.h"
#include "src/presentation/session_manager.h"
#include "src/presentation/timeline_container.h"

#include "src/logic/state_window_logic.h"

static GtkWidget* statewin = NULL;
static GtkWidget* label_user_menu = NULL;
static GtkWidget* image_user_menu = NULL;
static GtkWidget* stack_tl = NULL;
static GtkWidget* home_tl_container = NULL;
static GtkWidget* local_tl_container = NULL;
static GtkWidget* federated_tl_container = NULL;
static GtkWidget* tlcont_home = NULL;
static GtkWidget* tlcont_local = NULL;
static GtkWidget* tlcont_federated = NULL;

static void
__on_session_change(GtkWidget* caller, gpointer data)
{
    if (!statewin || !label_user_menu || !image_user_menu) {
        keno_error(
          KENO_ERR_NULL,
          "Tried to update user_menu_button's data but they're uninitialized");
        return;
    }

    // Check if there's an active session, really
    const Session* session =
      state_window_get_active_session(KENO_STATE_WINDOW(statewin));
    if (!session) {
        keno_error(
          KENO_ERR_NULL,
          "Tried to update user_menu_button's data but active session is NULL");
        return;
    }

    // Get account information (for username, not in session)
    kma_account* acc = kma_getLoggedAccount(session->conn);
    if (!acc) {
        keno_error(KENO_ERR_NULL,
                   "Couldn't retrieve user information to update MainWindow's "
                   "menu button.");
        return;
    }

    char* filename = account_get_avatar(acc);

    // Set info
    gtk_image_set_from_file(GTK_IMAGE(image_user_menu), filename);
    // gtk_label_set_text(sessmanrow->label_handle, session->handle);
    gtk_label_set_text(GTK_LABEL(label_user_menu), acc->display_name);

    // Free info
    free(filename);
    kma_destroyAccount(acc);
}

static void
__on_reload_view(GtkWidget* caller, TimelineContainer* tlcont)
{
    if (gtk_widget_is_visible(tlcont_home)) {
        timeline_container_reload(KENO_TIMELINE_CONTAINER(tlcont_home));
    }
    if (gtk_widget_is_visible(tlcont_local)) {
        timeline_container_reload(KENO_TIMELINE_CONTAINER(tlcont_local));
    }
    if (gtk_widget_is_visible(tlcont_federated)) {
        timeline_container_reload(KENO_TIMELINE_CONTAINER(tlcont_federated));
    }
}

// Show the post composing window
static void
__on_compose_post_clicked(GtkWidget* caller, GtkWidget* parent)
{
    if (parent == NULL) {
        return;
    }

    post_composer_init(parent, NULL);
}

void
main_window_init(GtkApplication* app, gpointer* data)
{
    ///////
    // GTK BUILDER
    /////

    // Make sure the GTK builder gets our custom classes
    g_type_ensure(TYPE_STATE_WINDOW);
    GtkBuilder* builder =
      gtk_builder_new_from_resource("/st/fai/keno/ui/main_window.ui");

    // Initializing global variables
    statewin = GTK_WIDGET(gtk_builder_get_object(builder, "window"));
    label_user_menu =
      GTK_WIDGET(gtk_builder_get_object(builder, "label_user_menu"));
    image_user_menu =
      GTK_WIDGET(gtk_builder_get_object(builder, "image_user_menu"));

    g_signal_connect(statewin,
                     "active_account_changed",
                     G_CALLBACK(__on_session_change),
                     NULL);

    ///////
    // Load CSS
    /////

    GtkCssProvider* css_provider = gtk_css_provider_new();
    gtk_css_provider_load_from_resource(css_provider, "st/fai/keno/app.css");
    gtk_style_context_add_provider_for_display(
      gtk_root_get_display(GTK_ROOT(statewin)),
      GTK_STYLE_PROVIDER(css_provider),
      GTK_STYLE_PROVIDER_PRIORITY_USER);

    ///////
    // Session Manager
    /////

    // Get container (popover)
    GtkWidget* box_popover_content =
      GTK_WIDGET(gtk_builder_get_object(builder, "box_popover_content"));

    // Create SessionManager and put into container
    GtkWidget* sessman = session_manager_new(statewin);
    gtk_box_append(GTK_BOX(box_popover_content), sessman);

    statewin = GTK_WIDGET(gtk_builder_get_object(builder, "window"));

    ///////
    // Session check
    /////

    Session** sessions = session_store_load();
    if (sessions == NULL || sessions[0] == NULL) {
        add_session_init(statewin, statewin);
    } else {
        __on_session_change(NULL, NULL);
    }
    destroy_session_list(sessions);

    ///////
    // Timeline
    /////

    // Creation of the three main timelines

    // HOME
    tlcont_home = timeline_container_new(statewin, get_home_timeline);
    home_tl_container =
      GTK_WIDGET(gtk_builder_get_object(builder, "home_tl_container"));
    gtk_frame_set_child(GTK_FRAME(home_tl_container), tlcont_home);

    timeline_container_reload(KENO_TIMELINE_CONTAINER(tlcont_home));

    // LOCAL
    tlcont_local = timeline_container_new(statewin, get_local_timeline);
    local_tl_container =
      GTK_WIDGET(gtk_builder_get_object(builder, "local_tl_container"));
    gtk_frame_set_child(GTK_FRAME(local_tl_container), tlcont_local);
    timeline_container_reload(KENO_TIMELINE_CONTAINER(tlcont_local));

    // FEDERATED
    tlcont_federated = timeline_container_new(statewin, get_federated_timeline);
    federated_tl_container =
      GTK_WIDGET(gtk_builder_get_object(builder, "federated_tl_container"));
    gtk_frame_set_child(GTK_FRAME(federated_tl_container), tlcont_federated);
    timeline_container_reload(KENO_TIMELINE_CONTAINER(tlcont_federated));

    GtkWidget* button_reload_view =
      GTK_WIDGET(gtk_builder_get_object(builder, "button_reload_view"));
    g_signal_connect(
      button_reload_view, "clicked", G_CALLBACK(__on_reload_view), NULL);

    // Post compose button
    GtkWidget* button_compose_post =
      GTK_WIDGET(gtk_builder_get_object(builder, "button_compose_post"));
    g_signal_connect(button_compose_post,
                     "clicked",
                     G_CALLBACK(__on_compose_post_clicked),
                     statewin);

    // GTKSTACK

    stack_tl = GTK_WIDGET(gtk_builder_get_object(builder, "main_stack"));

    ///////
    // GTKApplication things
    /////

    g_object_unref(builder);
    gtk_window_set_application(GTK_WINDOW(statewin), GTK_APPLICATION(app));
    gtk_widget_show(statewin);
}
