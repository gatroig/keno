#ifndef MAIN_WINDOW_CONTROLLER_H_
#define MAIN_WINDOW_CONTROLLER_H_

#include <gtk/gtk.h>

#include "../logic/session_logic.h"
// TODO: Move account image retrieval to logic
#include "../persistence/account_persistence.h"

#include "session_manager.h"

void
main_window_init(GtkApplication* app, gpointer* userdata);

#endif // MAIN_WINDOW_CONTROLLER_H_
