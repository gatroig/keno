#ifndef POST_COMPOSER_CONTROLLER_H_
#define POST_COMPOSER_CONTROLLER_H_

#include <gtk/gtk.h>

#include "src/logic/post_logic.h"

void
post_composer_init(GtkWidget* parent, Post* reply_post);

#endif // POST_COMPOSER_H_
