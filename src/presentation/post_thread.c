#include "post_thread.h"

#include "src/logic/state_window_logic.h"
#include "src/presentation/session_manager.h"
#include "src/utils.h"

static GtkWidget* window = NULL;

static const Session* session = NULL;

void
post_thread_init(GtkWidget* parent)
{
    if (parent == NULL) {
        keno_error(KENO_ERR_NULL, "Thread window could not get parent");
        return;
    }

    session = state_window_get_active_session(KENO_STATE_WINDOW(parent));

    if (session == NULL) {
        keno_error(KENO_ERR_NULL, "Thread window could not get session");
        return;
    }

    GtkBuilder* builder =
      gtk_builder_new_from_resource("/st/fai/keno/ui/timeline/thread.ui");

    window = GTK_WIDGET(gtk_builder_get_object(builder, "window"));

    g_object_unref(builder);

    gtk_window_set_transient_for(GTK_WINDOW(window), GTK_WINDOW(parent));
    gtk_window_set_modal(GTK_WINDOW(window), TRUE);
    gtk_widget_show(window);
}
