#include <stdio.h>
#include <stdlib.h>

#include <gtk/gtk.h>

#include <kma_auth.h>

#include "../logic/session_logic.h"
#include "../logic/state_window_logic.h"
#include "../persistence/session_persistence.h"
#include "../utils.h"

/**
 * Session creation process outline
 * 1. activate():
 *    - ask for instance
 * 2. on_instance_submit():
 *    - ask for user code after assembling URL from instance
 * 3. on_code_submit():
 *    - get token from code, terminate creating session
 */

void
add_session_init(GtkWidget* caller, GtkWidget* parent);
