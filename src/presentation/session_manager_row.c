#include "session_manager_row.h"
#include "session_manager.h"

struct _SessionManagerRow
{
    GtkListBoxRow parent;
    GtkImage* image_profile;
    GtkLabel* label_display_name;
    GtkLabel* label_handle;
    GtkButton* button_delete;
};

G_DEFINE_TYPE(SessionManagerRow, session_manager_row, GTK_TYPE_LIST_BOX_ROW);

#define __GET_SESSMAN(x)                                                       \
    KENO_SESSION_MANAGER(                                                      \
      gtk_widget_get_parent(gtk_widget_get_parent(GTK_WIDGET(x))));

static const Session*
__get_session(const SessionManagerRow* sessmanrow)
{
    // Get index, throw error if not under a SessionManager
    const int index = gtk_list_box_row_get_index(GTK_LIST_BOX_ROW(sessmanrow));
    if (index < 0) {
        keno_error(KENO_ERR_NULL,
                   "Tried to get session information from a unparented "
                   "SessionManagerRow.");
        return NULL;
    }

    // Get session, throw error if information couldn't be retrieved
    const SessionManager* sessman = __GET_SESSMAN(sessmanrow);
    const Session* session = session_manager_get_session_idx(sessman, index);
    if (!session) {
        keno_error(KENO_ERR_NULL,
                   "Failed to get session information for SessionManagerRow");
        return NULL;
    }

    return session;
}

struct accountInfoStruct
{
    SessionManagerRow* sessmanrow;
    GdkPixbuf* avatar;
    char* handle;
    char* display_name;
};

static int
__set_info_done(gpointer data)
{
    struct accountInfoStruct* params = data;
    gtk_image_set_from_pixbuf(params->sessmanrow->image_profile,
                              params->avatar);
    gtk_label_set_text(params->sessmanrow->label_handle, params->handle);
    gtk_label_set_text(params->sessmanrow->label_display_name,
                       params->display_name);

    // Free info
    g_object_unref(params->avatar);
    free(params->display_name);
    free(params->handle);
    g_free(params);

    return FALSE;
}

static void
__set_info_callback(GTask* task,
                    gpointer source_object,
                    gpointer task_data,
                    GCancellable* cancellable)
{
    if (g_task_return_error_if_cancelled(task)) {
        return;
    }

    const Session* session = __get_session(KENO_SESSION_MANAGER_ROW(task_data));
    if (!session) {
        keno_error(KENO_ERR_NULL,
                   "Couldn't update SessionManagerRow's session information");
        return;
    }

    // Get account information
    kma_account* acc = kma_getLoggedAccount(session->conn);
    if (!acc) {
        keno_error(
          KENO_ERR_NULL,
          "Couldn't retrieve user information for session manager row.");
        return;
    }

    // Set info

    // Profile picture
    char* filename = account_get_avatar(acc);
    GdkPixbuf* avi = gdk_pixbuf_new_from_file(filename, NULL);
    g_free(filename);

    struct accountInfoStruct* params =
      g_malloc(sizeof(struct accountInfoStruct));
    // We need the sessmanrow to update its widgets
    params->sessmanrow = KENO_SESSION_MANAGER_ROW(task_data);
    // Actual information
    params->avatar = avi;
    params->handle = strdup(session->handle);
    params->display_name = strdup(acc->display_name);
    kma_destroyAccount(acc);

    // Free operations should be done in __set_info_done()
    g_idle_add(__set_info_done, params);
}

void
session_manager_row_reload(SessionManagerRow* sessmanrow)
{
    // FIXME: Cancel previous job before starting a new one. Segfaults.
    GTask* task = g_task_new(NULL, NULL, NULL, sessmanrow);
    g_task_set_task_data(task, sessmanrow, NULL);
    g_task_run_in_thread(task, __set_info_callback);

    g_object_unref(task);
}

static void
session_manager_row_dispose(GObject* gobject)
{
    // We don't own the Session, it is the SessionManager.
    G_OBJECT_CLASS(session_manager_row_parent_class)->dispose(gobject);
}

static void
__on_delete(GtkWidget* button, GtkListBoxRow* row)
{
    if (!GTK_IS_LIST_BOX_ROW(row)) {
        keno_error(KENO_ERR_OTHER,
                   "Couldn't delete session, the data passed isn't a ROW.");
        return;
    }

    int i = gtk_list_box_row_get_index(row);
    if (i < 0) {
        keno_error(KENO_ERR_OTHER,
                   "Couldn't delete session. Selected row isn't in a list.");
        return;
    }

    const Session* session = __get_session(KENO_SESSION_MANAGER_ROW(row));
    if (session == NULL || session->handle == NULL) {
        keno_error(KENO_ERR_NULL, "Tried to remove invalid Session. Aborting.");
        return;
    }

    session_store_remove(session->handle);
    g_signal_emit_by_name(gtk_widget_get_root(button), "account_removed");
}

static void
session_manager_row_init(SessionManagerRow* sessmanrow)
{
    sessmanrow->button_delete = NULL;
    sessmanrow->image_profile = NULL;
    sessmanrow->label_display_name = NULL;
    sessmanrow->label_handle = NULL;

    gtk_widget_init_template(GTK_WIDGET(sessmanrow));

    g_signal_connect(sessmanrow->button_delete,
                     "clicked",
                     G_CALLBACK(__on_delete),
                     sessmanrow);
}

static void
session_manager_row_class_init(SessionManagerRowClass* class)
{
    GObjectClass* object_class = G_OBJECT_CLASS(class);

    object_class->dispose = session_manager_row_dispose;

    // Get template
    gtk_widget_class_set_template_from_resource(
      GTK_WIDGET_CLASS(class),
      "/st/fai/keno/ui/session/session_manager_row.ui");

    // Bind template objects
    gtk_widget_class_bind_template_child(
      GTK_WIDGET_CLASS(class), SessionManagerRow, label_display_name);
    gtk_widget_class_bind_template_child(
      GTK_WIDGET_CLASS(class), SessionManagerRow, label_handle);
    gtk_widget_class_bind_template_child(
      GTK_WIDGET_CLASS(class), SessionManagerRow, image_profile);
    gtk_widget_class_bind_template_child(
      GTK_WIDGET_CLASS(class), SessionManagerRow, button_delete);
}

GtkWidget*
session_manager_row_new()
{
    SessionManagerRow* sessmanrow =
      KENO_SESSION_MANAGER_ROW(g_object_new(TYPE_SESSION_MANAGER_ROW, NULL));

    return GTK_WIDGET(sessmanrow);
}
