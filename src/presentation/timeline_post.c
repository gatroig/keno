#include "timeline_post.h"

#include "../logic/state_window_logic.h"
// TODO: Change to LOGIC
#include "../persistence/account_persistence.h"
#include "../utils.h"
#include "kma_status.h"
#include "src/presentation/post_composer_controller.h"

struct _TimelinePost
{
    GtkBox parent;
    Post* post; // Owned by self

    GtkWidget* image_post_profile;
    GtkWidget* label_post_display_name;
    GtkWidget* label_post_handle;
    GtkWidget* label_post_body;
    GtkWidget* button_post_date;

    GtkWidget* button_post_locked;
    GtkWidget* box_post_reblog;
    GtkWidget* button_post_reblog;
    GtkWidget* label_post_reblog_count;
    GtkWidget* box_post_fav;
    GtkWidget* button_post_fav;
    GtkWidget* label_post_fav_count;
    GtkWidget* box_post_reply;
    GtkWidget* button_post_reply;

    GtkWidget* box_reblogged;
    GtkWidget* label_post_reblogged_by_display_name;
    GtkWidget* image_post_reblogged_by_profile;
};

G_DEFINE_TYPE(TimelinePost, timeline_post, GTK_TYPE_LIST_BOX_ROW);

void
__cb_show_thread(GtkWidget* caller, Post* post)
{
    if (post == NULL) {
        keno_error(KENO_ERR_OTHER, "Post is NULL");
        return;
    }
    puts("Date Clicked");
    GtkWidget* statewin = GTK_WIDGET(gtk_widget_get_root(caller));
    post_thread_init(statewin);
}

static void
timeline_post_dispose(GObject* gobject)
{
    TimelinePost* tlpost = KENO_TIMELINE_POST(gobject);
    destroy_post(tlpost->post);
    tlpost->post = NULL;
    G_OBJECT_CLASS(timeline_post_parent_class)->dispose(gobject);
}

static void
timeline_post_init(TimelinePost* tlpost)
{
    tlpost->post = NULL;
    gtk_widget_init_template(GTK_WIDGET(tlpost));
}

static void
timeline_post_class_init(TimelinePostClass* class)
{
    GObjectClass* object_class = G_OBJECT_CLASS(class);

    object_class->dispose = timeline_post_dispose;

    // Get template
    gtk_widget_class_set_template_from_resource(
      GTK_WIDGET_CLASS(class), "/st/fai/keno/ui/timeline/timeline_post.ui");
    // Bind template objects
    gtk_widget_class_bind_template_child(
      GTK_WIDGET_CLASS(class), TimelinePost, image_post_profile);
    gtk_widget_class_bind_template_child(
      GTK_WIDGET_CLASS(class), TimelinePost, label_post_display_name);
    gtk_widget_class_bind_template_child(
      GTK_WIDGET_CLASS(class), TimelinePost, label_post_handle);
    gtk_widget_class_bind_template_child(
      GTK_WIDGET_CLASS(class), TimelinePost, label_post_body);
    gtk_widget_class_bind_template_child(
      GTK_WIDGET_CLASS(class), TimelinePost, button_post_date);

    gtk_widget_class_bind_template_child(
      GTK_WIDGET_CLASS(class), TimelinePost, button_post_locked);
    gtk_widget_class_bind_template_child(
      GTK_WIDGET_CLASS(class), TimelinePost, box_post_reblog);
    gtk_widget_class_bind_template_child(
      GTK_WIDGET_CLASS(class), TimelinePost, button_post_reblog);
    gtk_widget_class_bind_template_child(
      GTK_WIDGET_CLASS(class), TimelinePost, label_post_reblog_count);
    gtk_widget_class_bind_template_child(
      GTK_WIDGET_CLASS(class), TimelinePost, box_post_fav);
    gtk_widget_class_bind_template_child(
      GTK_WIDGET_CLASS(class), TimelinePost, button_post_fav);
    gtk_widget_class_bind_template_child(
      GTK_WIDGET_CLASS(class), TimelinePost, label_post_fav_count);
    gtk_widget_class_bind_template_child(
      GTK_WIDGET_CLASS(class), TimelinePost, box_post_reply);
    gtk_widget_class_bind_template_child(
      GTK_WIDGET_CLASS(class), TimelinePost, button_post_reply);

    gtk_widget_class_bind_template_child(
      GTK_WIDGET_CLASS(class), TimelinePost, box_reblogged);
    gtk_widget_class_bind_template_child(GTK_WIDGET_CLASS(class),
                                         TimelinePost,
                                         label_post_reblogged_by_display_name);
    gtk_widget_class_bind_template_child(
      GTK_WIDGET_CLASS(class), TimelinePost, image_post_reblogged_by_profile);
}

static void
__gtk_label_set_int(GtkLabel* label, int num)
{
    char* count_err = "ERR";

    char* str_int = NULL;
    asprintf(&str_int, "%d", num);
    if (str_int == NULL)
        str_int = count_err;
    gtk_label_set_label(GTK_LABEL(label), str_int);

    // Free the string only if not the static error string
    if (str_int != count_err)
        free(str_int);
}

static void
__perform_action(GtkWidget* caller,
                 TimelinePost* tlpost,
                 const char* action_str)
{
    StateWindow* statewin = KENO_STATE_WINDOW(gtk_widget_get_root(caller));
    if (statewin == NULL || state_window_get_active_session(statewin) == NULL) {
        keno_error(
          KENO_ERR_NULL,
          "Failed to perform action on post, couldn't get current app state");
        return;
    }

    // Perform action
    kma_status* updated_status = NULL;
    int ret = kma_statusAction(state_window_get_active_session(statewin)->conn,
                               tlpost->post->status->id,
                               &updated_status,
                               action_str);

    // Update the post
    if (ret && updated_status != NULL) {
        // visuals
        // TODO: Put the opacity in a CSS class
        gtk_widget_set_opacity(GTK_WIDGET(tlpost->box_post_reblog),
                               updated_status->reblogged ? 1.0 : 0.33);
        __gtk_label_set_int(GTK_LABEL(tlpost->label_post_reblog_count),
                            updated_status->reblogs_count);
        gtk_widget_set_opacity(GTK_WIDGET(tlpost->box_post_fav),
                               updated_status->favourited ? 1.0 : 0.33);
        __gtk_label_set_int(GTK_LABEL(tlpost->label_post_fav_count),
                            updated_status->favourites_count);

        // Replace previous status with current one
        kma_destroyStatus(tlpost->post->status);
        tlpost->post->status = updated_status;
        updated_status = NULL;
    } else {
        keno_error(KENO_ERR_OTHER, "Failed to reblog post, request failed.");
    }
}

static void
__cb_reblog_post(GtkWidget* caller, TimelinePost* tlpost)
{
    if (tlpost == NULL || tlpost->post == NULL ||
        tlpost->post->status == NULL) {
        keno_error(KENO_ERR_NULL, "Failed to reblog post, NULL was passed.");
        return;
    }

    __perform_action(caller,
                     tlpost,
                     tlpost->post->status->reblogged
                       ? KMA_STATUS_ACTION_UNREBLOG
                       : KMA_STATUS_ACTION_REBLOG);
}

static void
__cb_fav_post(GtkWidget* caller, TimelinePost* tlpost)
{
    if (tlpost == NULL || tlpost->post == NULL ||
        tlpost->post->status == NULL) {
        keno_error(KENO_ERR_NULL, "Failed to fav post, NULL was passed.");
        return;
    }

    __perform_action(caller,
                     tlpost,
                     tlpost->post->status->favourited ? KMA_STATUS_ACTION_UNFAV
                                                      : KMA_STATUS_ACTION_FAV);
}

static void
__cb_reply_post(GtkWidget* caller, TimelinePost* tlpost)
{
    if (tlpost == NULL || tlpost->post == NULL ||
        tlpost->post->status == NULL || tlpost->post->status->id == NULL) {
        keno_error(KENO_ERR_NULL, "Failed to fav post, NULL was passed.");
        return;
    }

    GtkRoot* root = gtk_widget_get_root(caller);
    if (root == NULL) {
        keno_error(KENO_ERR_OTHER,
                   "Couldn't reply to post because fetching the root failed");
        return;
    }

    post_composer_init(GTK_WIDGET(root), tlpost->post);
}

struct setphoto_data
{
    TimelinePost* tlpost;
    char* avi_author;
    char* avi_reblogged_by;
};

static int
__idle_set_photos(gpointer data)
{
    struct setphoto_data* sp_data = (struct setphoto_data*)data;

    if (sp_data->avi_reblogged_by != NULL) {
        gtk_image_set_from_file(
          GTK_IMAGE(sp_data->tlpost->image_post_reblogged_by_profile),
          sp_data->avi_reblogged_by);
    }
    gtk_image_set_from_file(GTK_IMAGE(sp_data->tlpost->image_post_profile),
                            sp_data->avi_author);

    return 0;
}

void
timeline_post_make_post(TimelinePost* tlpost, Post* post)
{
    if (tlpost == NULL) {
        keno_error(KENO_ERR_NULL, "Tried to reload NULL timeline.");
        return;
    }

    StateWindow* statewin =
      KENO_STATE_WINDOW(gtk_widget_get_root(GTK_WIDGET(tlpost)));
    if (statewin == NULL) {
        keno_error(KENO_ERR_NULL,
                   "Tried to reaload timeline with a NULL root. Maybe still "
                   "not added to a cointainer?");
        return;
    }

    tlpost->post = post;

    // Assemble Post
    ////////

    gtk_label_set_label(GTK_LABEL(tlpost->label_post_handle),
                        post->status->account->acct);
    gtk_label_set_label(GTK_LABEL(tlpost->label_post_display_name),
                        post->status->account->display_name);
    gtk_label_set_label(GTK_LABEL(tlpost->label_post_body),
                        post->status->content);

    // Action buttons
    ////////

    // Reblog action button with count (or lock if non rebloggable post)
    if (strcmp(post->status->visibility, KMA_STATUS_VISIBILITY_PUBLIC) == 0 ||
        strcmp(post->status->visibility, KMA_STATUS_VISIBILITY_UNLISTED) == 0) {

        if (post->status->reblogged)
            gtk_widget_set_opacity(tlpost->box_post_reblog, 1.0);
        __gtk_label_set_int(GTK_LABEL(tlpost->label_post_reblog_count),
                            post->status->reblogs_count);
        // Connect button
        g_signal_connect(tlpost->button_post_reblog,
                         "clicked",
                         G_CALLBACK(__cb_reblog_post),
                         tlpost);
    } else {
        gtk_widget_set_visible(tlpost->button_post_reblog, 0);
        gtk_widget_set_visible(tlpost->button_post_locked, 1);
    }

    // Fav action button with count
    if (post->status->favourited)
        gtk_widget_set_opacity(tlpost->box_post_fav, 1.0);
    __gtk_label_set_int(GTK_LABEL(tlpost->label_post_fav_count),
                        post->status->favourites_count);
    // Connect button
    g_signal_connect(
      tlpost->button_post_fav, "clicked", G_CALLBACK(__cb_fav_post), tlpost);
    g_signal_connect(tlpost->button_post_reply,
                     "clicked",
                     G_CALLBACK(__cb_reply_post),
                     tlpost);

    // Reblog header
    ////////

    // Check if post is a reblog
    if (post->reblogged_by != NULL) {
        gtk_widget_set_visible(tlpost->box_reblogged, TRUE);
        gtk_label_set_label(
          GTK_LABEL(tlpost->label_post_reblogged_by_display_name),
          post->reblogged_by->display_name);
    }

    // Date
    ////////
    g_signal_connect(tlpost->button_post_date,
                     "clicked",
                     G_CALLBACK(__cb_show_thread),
                     tlpost->post);
    if (tlpost->post->status->created_at == NULL) {
        keno_error(KENO_ERR_NULL, "Post has no date");
    } else {
        gtk_button_set_label(GTK_BUTTON(tlpost->button_post_date),
                             tlpost->post->status->created_at);
    }

    // Set images (in another thread)
    ////////

    struct setphoto_data* data = malloc(sizeof(struct setphoto_data));
    if (data == NULL) {
        keno_error(KENO_ERR_MEMORY,
                   "Couldn't allocate space for post's image paths.");
    } else {
        data->avi_author = account_get_avatar(tlpost->post->status->account);
        data->tlpost = tlpost;

        if (tlpost->post->reblogged_by != NULL) {
            data->avi_reblogged_by =
              account_get_avatar(tlpost->post->reblogged_by);
        } else {
            data->avi_reblogged_by = NULL;
        }

        g_idle_add(__idle_set_photos, data);
    }
}

GtkWidget*
timeline_post_new()
{
    TimelinePost* tlpost = g_object_new(TYPE_TIMELINE_POST, NULL);

    // Set pointer cursor on fav and rt buttons
    GdkCursor* fallback_cursor = gdk_cursor_new_from_name("default", NULL);
    GdkCursor* pointer_cursor =
      gdk_cursor_new_from_name("pointer", fallback_cursor);
    gtk_widget_set_cursor(tlpost->button_post_fav, pointer_cursor);
    gtk_widget_set_cursor(tlpost->button_post_reblog, pointer_cursor);
    gtk_widget_set_cursor(tlpost->button_post_reply, pointer_cursor);
    gtk_widget_set_cursor(tlpost->button_post_date, pointer_cursor);

    return GTK_WIDGET(tlpost);
}
