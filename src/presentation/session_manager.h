#ifndef SESSION_MANAGER_H_
#define SESSION_MANAGER_H_

#include <gtk/gtk.h>

#include "../logic/session_logic.h"
#include "../logic/state_window_logic.h"
#include "../persistence/session_persistence.h"

#include "add_session_controller.h"

#define TYPE_SESSION_MANAGER session_manager_get_type()
G_DECLARE_FINAL_TYPE(SessionManager,
                     session_manager,
                     KENO,
                     SESSION_MANAGER,
                     GtkBox)

GtkWidget*
session_manager_new(GtkWidget* root);

void
session_manager_reload(SessionManager* sessman);
void
session_manager_clear(SessionManager* sessman);

Session*
session_manager_get_session_idx(const SessionManager* sessman, int index);

#endif // SESSION_MANAGER_H_
