#ifndef SESSION_MANAGER_ROW_H_
#define SESSION_MANAGER_ROW_H_

#include <gtk/gtk.h>

#include <kma_account.h>

#include "../logic/session_logic.h"
#include "../persistence/account_persistence.h"
#include "../persistence/session_persistence.h"

#define TYPE_SESSION_MANAGER_ROW session_manager_row_get_type()
G_DECLARE_FINAL_TYPE(SessionManagerRow,
                     session_manager_row,
                     KENO,
                     SESSION_MANAGER_ROW,
                     GtkListBoxRow)

GtkWidget*
session_manager_row_new();

void
session_manager_row_reload(SessionManagerRow* sessmanrow);

#endif // SESSION_MANAGER_ROW_H_
