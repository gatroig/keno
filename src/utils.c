#include "utils.h"

void
keno_error(char* type, char* msg)
{
    fprintf(
      stderr, "[\e[0;31m%s\e[0m] of type %s: %s\n", KENO_ERR_PREFIX, type, msg);
}

void
keno_warning(char* type, char* msg)
{
    fprintf(
      stderr, "[\e[0;33m%s\e[0m] of type %s: %s\n", WARNING_PREFIX, type, msg);
}
