#ifndef ACCOUNT_PERSISTENCE_H
#define ACCOUNT_PERSISTENCE_H
#define _GNU_SOURCE

#include <gtk/gtk.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <kma_account.h>

#include "../utils.h"
#include "utils_persistence.h"

char*
account_get_avatar(const kma_account* account);

#endif // ACCOUNT_PERSISTENCE_H
