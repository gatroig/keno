#ifndef CONNECTION_PERSISTENCE_H
#define CONNECTION_PERSISTENCE_H
#define _GNU_SOURCE

#include <stdio.h>

#include "../utils.h"
#include "utils_persistence.h"

// Nothing here since the migration of JSON to SQLite3...

#endif // CONNECTION_PERSISTENCE_H
