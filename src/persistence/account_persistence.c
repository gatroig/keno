#include "account_persistence.h"

char*
account_get_avatar(const kma_account* account)
{
    if (!account || !account->avatar || !account->username) {
        keno_error(KENO_ERR_NULL,
                   "Tried to get profile picture from invalid account.");
        return NULL;
    }

    // Get folder to put the image in
    char* userdir = get_data_dir();
    char* folder = g_build_filename(userdir, "avatar", account->username, NULL);
    g_free(userdir);

    // Create dir with parents if necessary
    g_mkdir_with_parents(folder, 0700);

    char* hash = hash_string(account->avatar);
    if (!hash) {
        g_free(folder);
        keno_error(KENO_ERR_NULL, "Couldn't get hash for avatar filename.");
        return NULL;
    }

    char* filename = g_build_filename(folder, hash, NULL);
    g_free(folder);
    free(hash);

    // If file doesn't exist, create. If it does, do nothing.
    if (access(filename, F_OK) != 0) {
        http_download_img_scale(account->avatar, filename, 64, 64);
    }
    char* ret = strdup(filename);
    g_free(filename);

    return ret;
}
