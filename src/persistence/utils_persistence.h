#ifndef UTILS_PERSISTENCE_H
#define UTILS_PERSISTENCE_H

#include <curl/curl.h>
#include <errno.h>
#include <sqlite3.h>
#include <stdio.h>

#include <gdk-pixbuf/gdk-pixbuf.h>
#include <gio/gio.h>
#include <glib.h>

#include "../utils.h"

// ------------------- FILES ------------------------

/** Gets the user's data directory, uses glib */
char*
get_data_dir();

/**
 * Get file contents from a path.
 * @param Path of the file to read
 * @param Sets buffer to the contents of the file.
 *
 * @return KENO_OK if eveyrthing went well, ENOENT if the file
 * doesn't exist and ERROR in any other case.
 */

#define KENO_UTILS_FILE_ENOENT 1
#define KENO_UTILS_FILE_ERROR 2

int
get_file_contents(char* path, char** buffer);

char*
hash_string(const char* string);

// ------------------- SQLite3 ----------------------

/**
 * Get the application database.
 * @return Returns NULL on error
 */
sqlite3*
get_db();

// -------------------- HTTP ------------------------

int
http_download_img(char* url, char* path);

GInputStream*
http_get_gio_input_stream(const char* url);

int
http_download_img_scale(const char* url,
                        const char* path,
                        int width,
                        int height);

#endif // UTILS_PERSISTENCE_H
