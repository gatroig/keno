#include "utils_persistence.h"

// ------------------- FILES ------------------------

char*
get_data_dir()
{
    // Will not attempt to create again if it exists already
    char* usrdir =
      g_build_filename(g_get_user_data_dir(), APPLICATION_NAME, NULL);

    // Only the pertintent user should be able to see this.
    // permissions must be in octal, that's why we prepend the 0
    g_mkdir_with_parents(usrdir, 0700);

    return usrdir;
}

int
get_file_contents(char* path, char** buffer)
{
    FILE* f = fopen(path, "r");

    if (!f) {
        if (errno == ENOENT)
            // Not existing might be something fine actually, we may want to
            // just create in that case, like with config files
            return KENO_UTILS_FILE_ENOENT;
        else {
            keno_error(
              KENO_ERR_FILE,
              "Could not open file for reading. No read permissions maybe?");
            return KENO_UTILS_FILE_ERROR;
        }
    }

    // with ftell() we can get where we are in the file and with fseek we can
    // tell it to go to the start of the file. Using that, we can now how big
    // the file is to read the whole thing.
    fseek(f, 0, SEEK_END);
    long fsize = ftell(f);
    // Go to the start, as we need to read the thing now.
    fseek(f, 0, SEEK_SET);

    (*buffer) = malloc(sizeof(char) * (fsize + 1));
    if (!(*buffer)) {
        fclose(f);
        keno_error(KENO_ERR_MEMORY,
                   "Could not allocate memory to read file's contents");
        return 0;
    }

    // We can actually read the file now. We have to null terminate it as it
    // isn't by default.
    fread((*buffer), fsize, 1, f);
    (*buffer)[fsize] = '\0';
    fclose(f);

    return 1;
}

char*
hash_string(const char* string)
{
    // Glib hashes are always 10 characters long. I hope.
    char* ret = malloc(sizeof(char) * 11);
    if (!ret)
        return NULL;

    sprintf(ret, "%u", g_str_hash(string));
    return ret;
}

// ------------------ SQLite3 -----------------------

sqlite3*
get_db()
{
    // Get database path
    char* userdir = get_data_dir();
    char* path = g_build_filename(userdir, "data.db", NULL);
    g_free(userdir);

    // Get database pointer
    sqlite3* db = NULL;
    int ret = sqlite3_open_v2(path,
                              &db,
                              SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE |
                                SQLITE_OPEN_FULLMUTEX,
                              NULL);
    g_free(path);

    if (ret != SQLITE_OK) {
        keno_error(KENO_ERR_FILE, "Couldn't retrieve/create databse.");
        return NULL;
    }

    return db;
}

// -------------------- HTTP ------------------------

static size_t
__write_file_callback(void* ptr, size_t size, size_t nmemb, void* stream)
{
    return fwrite(ptr, size, nmemb, (FILE*)stream);
}

int
http_download_img(char* url, char* path)
{
    CURL* curl;
    FILE* file;

    curl_global_init(CURL_GLOBAL_ALL);
    curl = curl_easy_init();
    if (curl) {
        curl_easy_setopt(curl, CURLOPT_URL, url);
        curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 1L);
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, __write_file_callback);

        file = fopen(path, "wb");
        if (file) {
            curl_easy_setopt(curl, CURLOPT_WRITEDATA, file);
            curl_easy_perform(curl);

            fclose(file);
        } else {
            return KENO_ERROR;
        }
    } else {
        return KENO_ERROR;
    }

    curl_easy_cleanup(curl);
    curl_global_cleanup();

    return KENO_OK;
}

// Struct to help the assembly of the returned data
// as it may be retuned in chunks
struct callback_state
{
    int prev_size;
    char* data;
};

// Callback function to write data
static size_t
__write_data(void* contents, size_t size, size_t nmemb, void* userp)
{
    // Cast pointer to the passed struct
    struct callback_state* state = userp;

    // If this is true, it's the first iteration
    if (!state->data) {
        // No need for the  +1, there's no \0
        state->data = malloc(nmemb * (size));
        if (!state->data)
            return 1;
    }
    // Subsequent calls to this callback
    else {
        // We're calling realloc with the size it had in the previous
        // iteration+the size curl is telling us that this chunk has
        state->data =
          realloc(state->data, state->prev_size + (nmemb * (size + 1)));
    }

    // Copy the actual data in the proper offset (prev_size is 0 at first
    // iteration, so it is safe to put it here)
    memcpy(state->data + state->prev_size, contents, nmemb * size);
    // After every iteration put '\0' at the end, as we don't know if this
    // is our last time on this green earth
    // state->data[state->prev_size + (size * nmemb)] = '\0';
    // Update the size so the next iteration knows where we left off
    state->prev_size += size * nmemb;

    return size * nmemb;
}

GInputStream*
http_get_gio_input_stream(const char* url)
{
    CURL* curl;
    CURLcode res = CURLE_GOT_NOTHING;

    // Initialize curl
    curl_global_init(CURL_GLOBAL_ALL);
    curl = curl_easy_init();

    // Returned string, response to request
    struct callback_state callback_ret = { 0, NULL };

    // If succesful
    if (curl) {
        curl_easy_setopt(curl, CURLOPT_URL, url);
        curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 1L);

        // Assign callback function and where to write the return data
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, __write_data);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, &callback_ret);

        res = curl_easy_perform(curl);
        if (res != CURLE_OK) {
            fprintf(stderr,
                    "curl_easy_perform() failed: %s\n",
                    curl_easy_strerror(res));
        }
    }

    curl_global_cleanup();

    if (res != CURLE_OK)
        return KENO_ERROR;

    return g_memory_input_stream_new_from_data(
      callback_ret.data, callback_ret.prev_size, NULL);
}

int
http_download_img_scale(const char* url,
                        const char* path,
                        int width,
                        int height)
{
    if (url == NULL || path == NULL) {
        keno_error(KENO_ERR_NULL, "Couldn't retrieve image. Invalid call.");
        return KENO_ERROR;
    }

    GInputStream* stream = http_get_gio_input_stream(url);
    if (stream == NULL) {
        keno_error(KENO_ERR_NULL, "Couldn't create stream from request.");
        return KENO_ERROR;
    }

    // TODO: See if the stream has to be freed
    GdkPixbuf* pb_orig = gdk_pixbuf_new_from_stream(stream, NULL, NULL);
    if (pb_orig == NULL) {
        keno_error(KENO_ERR_NULL, "Couldn't create image from stream.");
        return KENO_ERROR;
    }

    GdkPixbuf* pb_scaled =
      gdk_pixbuf_scale_simple(pb_orig, width, height, GDK_INTERP_BILINEAR);
    // original pixbuf isn't touched, we gotta free it
    g_object_unref(pb_orig);
    if (pb_scaled == NULL) {
        keno_error(KENO_ERR_OTHER, "Couldn't resize image.");
        return KENO_ERROR;
    }

    gdk_pixbuf_save(pb_scaled, path, "jpeg", NULL, "quality", "80", NULL);
    g_object_unref(pb_scaled);

    return KENO_OK;
}
