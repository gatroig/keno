#include <gtk/gtk.h>

#include "presentation/main_window_controller.h"

#include "persistence/utils_persistence.h"

int
main(int argc, char** argv)
{
    GtkApplication* app;
    int status;

    app = gtk_application_new("st.fai.keno", G_APPLICATION_FLAGS_NONE);
    g_signal_connect(app, "activate", G_CALLBACK(main_window_init), NULL);
    status = g_application_run(G_APPLICATION(app), argc, argv);
    g_object_unref(app);

    return status;
}
