#ifndef UTILS_H_
#define UTILS_H_

#include <stdio.h>

#define KENO_OK 1
#define KENO_ERROR 0

#define KENO_ERR_PREFIX "KENOMA_ERR"
#define WARNING_PREFIX "KENOMA_WARNING"

#define KENO_ERR_MEMORY "ERR_MEMORY"
#define KENO_ERR_NULL "ERR_NULL_VALUE"
#define KENO_ERR_CRITICAL "ERR_CRITICAL"
#define KENO_ERR_FILE "ERR_FILE"
#define KENO_ERR_OTHER "ERR_OTHER"
#define KENO_ERR_CONSTRAINT "ERR_CONSTRAINT_VIOLATION"

void
keno_error(char* type, char* msg);
void
keno_warning(char* type, char* msg);

#endif // UTILS_H_
