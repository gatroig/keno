# keno

Mastodon/Pleroma GTK4 graphical client aiming for
performance and reduced footprint.

Keno is a two-part project consisting of this
graphical frontend to the
[libkenoma library](https://codeberg.org/gatroig/libkenoma).

## Installation

As this software is an early work in progress,
it's not avaliable in any type of package.
You can, however, compile it from source. To do
that please refer to [building](#building)

### Appimage

Soon™

### Flatpak

Soon™

### AUR

Soon™

## Dependencies

The graphical client depends on

```
gtk4 libcurl sqlite3
```

While the libkenoma library needs

```
libcurl json-c
```

These dependencies and other required packages can
be installed under Arch Linux with the following
command as root:

``` shell
# pacman -S meson base-devel curl json-c gtk4 sqlite3
```

## Building

Run the following commands:

``` shell
$ meson build
$ meson compile -C build
```

## License

This software is under the GPL3 license. See LICENSE.
